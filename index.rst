.. Recetas Docker documentation master file, created by
   sphinx-quickstart on Thu Aug 31 10:27:17 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Recetas Docker's documentation!
==========================================

.. toctree::
   :maxdepth: 2

   capitulo_1
   capitulo_2

Licencia
--------

.. toctree::
   :maxdepth: 1

   licencia
